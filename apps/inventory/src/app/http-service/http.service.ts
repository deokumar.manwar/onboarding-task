import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor() {}

  async getData() {
    const response = await fetch('http://localhost:3000');
    const data = await response.json();
    console.log(data);
    return data;
  }
}
