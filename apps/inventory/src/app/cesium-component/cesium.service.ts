import { Injectable } from '@angular/core';
import * as Cesium from 'cesium';

Cesium.Ion.defaultAccessToken =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2MjliMmNkNC0xZGZkLTQ5MWUtYjNiMS03MmNjZTFjMzQ1MDQiLCJpZCI6MTkyNzk4LCJpYXQiOjE3MDY2MDA3NzR9.XR7AybGhGcDjfVkZvqdZlMJ3Zv8NhG_6MlmiRkKy_Lg';

@Injectable({
  providedIn: 'root',
})
export class CesiumService {
  private _latitude: number = 73.765925;
  private _longitude: number = 18.650923;
  private _height: number = 100;
  private viewer: any;
  private objectEntity: any;

  constructor() {}

  public get latitude(): number {
    return this._latitude;
  }

  public set latitude(value: number) {
    this._latitude = value;
    this.updatePointPosition();
  }

  public get longitude(): number {
    return this._longitude;
  }

  public set longitude(value: number) {
    this._longitude = value;
    this.updatePointPosition();
  }

  public get height(): number {
    return this._height;
  }

  public set height(value: number) {
    this._height = value;
    this.updatePointPosition();
  }

  initializeViewer(div: string) {
    this.viewer = new Cesium.Viewer(div, {
      infoBox: false,
      selectionIndicator: false,
      shadows: true,
      shouldAnimate: true,
    });
  }

  createModel(filename: string) {
    if (!this.viewer) {
      console.error(
        'Viewer not initialized. Call initializeViewer method first.'
      );
      return;
    }

    console.log(this.latitude, this.longitude, this.height);
    const url = '../../assets/' + filename;
    const position = Cesium.Cartesian3.fromDegrees(
      this.latitude,
      this.longitude,
      this.height
    );
    const heading = Cesium.Math.toRadians(135);
    const pitch = 0;
    const roll = 0;
    const hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
    const orientation = Cesium.Transforms.headingPitchRollQuaternion(
      position,
      hpr
    );

    this.objectEntity = this.viewer.entities.add({
      name: filename,
      position: position,
      orientation: orientation,
      model: {
        uri: url,
        minimumPixelSize: 128,
        maximumScale: 20000,
      },
    });

    this.viewer.trackedEntity = this.objectEntity;
  }

  private updatePointPosition() {
    if (this.objectEntity) {
      this.objectEntity.position = Cesium.Cartesian3.fromDegrees(
        this.latitude,
        this.longitude,
        this.height
      );
    }
  }
}
