import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CesiumService } from './cesium.service';

@Component({
  selector: 'app-cesium-component',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './cesium-component.component.html',
  styleUrl: './cesium-component.component.css',
})
export class CesiumComponentComponent implements OnInit {
  @Input() latitude!: number;
  @Input() longitude!: number;
  @Input() height!: number;

  constructor(private cesium: CesiumService) {
  }

  ngOnInit(): void {
    this.cesium.initializeViewer('cesium');
    this.cesium.createModel('CesiumDrone.glb');
  }
}
