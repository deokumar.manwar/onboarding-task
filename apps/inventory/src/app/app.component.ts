import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { CesiumService } from './cesium-component/cesium.service';
import { CesiumComponentComponent } from './cesium-component/cesium-component.component';
import { FormsModule } from '@angular/forms';
import { WebSocketService } from './websocket-service/web-socket.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    RouterOutlet,
    RouterModule,
    CesiumComponentComponent,
  ],
  templateUrl: `./app.component.html`,
})
export class AppComponent implements OnInit {
  latitude: number;
  longitude: number;
  height: number = 100;

  constructor(
    private cesiumService: CesiumService,
    private webSocketService: WebSocketService
  ) {
    this.latitude = this.cesiumService.latitude;
    this.longitude = this.cesiumService.longitude;
    this.height = this.cesiumService.height;
  }

  ngOnInit() {
    this.webSocketService.listen('onMessage').subscribe((data: any) => {
      console.log(data);
      let newData;
      if (typeof data.content === 'string') {
        newData = JSON.parse(data.content);
      } else {
        newData = data.content;
      }
      this.cesiumService.latitude = newData.latitude;
      this.cesiumService.height = newData?.height || 100;
      this.cesiumService.longitude = newData.longitude;
      this.latitude = newData.latitude;
      this.longitude = newData.longitude;
      this.height = newData?.height || 100;
    });
  }

  updatePosition() {
    console.log('updatePosition', this.latitude, this.longitude, this.height);
    this.webSocketService.emit('newMessage', {
      latitude: this.latitude,
      longitude: this.longitude,
      height: this.height,
    });
  }
}
