import { Directive, OnInit, ElementRef } from '@angular/core';
import {
  Cartesian3,
  Color,
  Viewer,
  Entity,
  ConstantPositionProperty,
} from 'cesium';

@Directive({
  selector: '[appCesium]',
  standalone: true,
})
export class CesiumDirective implements OnInit {
  private viewer!: Viewer;
  private entity!: Entity;
  latitude!: number;
  longitude!: number;

  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    this.viewer = new Viewer(this.el.nativeElement);

    // Initial position
    const initialPosition = Cartesian3.fromDegrees(18.650923, 73.765925);

    // Create a ConstantPositionProperty with the initial position
    const positionProperty = new ConstantPositionProperty(initialPosition);

    // Add a point to the map
    this.entity = this.viewer.entities.add({
      position: positionProperty,
      point: {
        pixelSize: 10,
        color: Color.RED,
      },
    });
  }

  updatePosition(): void {
    const longitude = parseFloat(this.longitude.toString());
    const latitude = parseFloat(this.latitude.toString());
    if (!isNaN(longitude) && !isNaN(latitude)) {
      const newPosition = Cartesian3.fromDegrees(latitude, longitude);

      // Update the position property of the entity
      this.entity.position = new ConstantPositionProperty(newPosition);
    } else {
      alert('Invalid latitude or longitude.');
    }
  }
}
