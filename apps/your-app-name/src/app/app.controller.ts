import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MqttService } from './mqtt.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly mqttService: MqttService
  ) {
    this.mqttService.subscribe(
      '65a9faee612c81ed9fd2c9f0/65a9fc72df54b0f97961a96e/#'
    );
    this.mqttService.onMessage((topic, message) => {
      this.getNotifications({ topic, message });
    });
  }

  @Get()
  getData(): string {
    this.appService.onMessageMqttEmit({
      latitude: 18.650923,
      longitude: 73.765925,
    });
    return 'This is Socket.io Test API';
  }

  getNotifications(data: any): void {
    try {
      this.appService.onMessageMqttEmit(data?.position);
    } catch (e) {
      console.error(e);
    }
  }
}
