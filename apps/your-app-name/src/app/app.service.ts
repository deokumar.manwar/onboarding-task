import { OnModuleInit } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'socket.io';

@WebSocketGateway({
  cors: {
    origin: 'http://localhost:4200',
  },
})
export class AppService implements OnModuleInit {
  @WebSocketServer()
  server: Server;
  rooms: any;

  onModuleInit(): void {
    this.server.on('connection', (socket) => {
      console.log(socket.rooms);
      this.rooms = socket.rooms;
      console.log('connected....');
    });
  }

  @SubscribeMessage('newMessage')
  onMessageEmit(@MessageBody() body: any): void {
    console.log('was here', body);
    if (!body) {
      console.log('undefined Data');
      return;
    }
    this.server.emit('onMessage', {
      msg: 'New message',
      content: body,
    });
  }

  onMessageMqttEmit(data: any): void {
    console.log('emited', data);
    if (!data) {
      console.log('undefined Data');
      return;
    }
    this.server.emit('onMessage', {
      msg: 'New message',
      content: data,
    });
  }
}
