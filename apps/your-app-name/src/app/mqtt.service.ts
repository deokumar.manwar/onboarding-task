import { Injectable } from '@nestjs/common';
import * as mqtt from 'mqtt';

@Injectable()
export class MqttService {
  private client: mqtt.MqttClient;

  constructor() {
    const options = {
      clientId: process.env.clientId,
      username: 'fb_dji_cloud_bridge',
      password: process.env.password,
    };

    const brokerUrl = process.env.connectUrl;

    this.client = mqtt.connect(brokerUrl, options);

    this.client.on('connect', () => {
      console.log('Connected to MQTT broker');
    });

    this.client.on('error', (error) => {
      console.error('Error occurred:', error);
    });

    this.client.on('close', () => {
      console.log('Connection to MQTT broker closed');
    });

    this.client.on('offline', () => {
      console.log('Client is offline');
    });

    this.client.on('end', () => {
      console.log('Client has ended');
    });

    this.client.on('disconnect', () => {
      console.log('Client has disconnected');
    });
  }

  subscribe(topic: string) {
    this.client.subscribe(topic, (err) => {
      if (err) {
        console.error('Error subscribing to topic:', err);
      } else {
        console.log(`Subscribed to ${topic}`);
      }
    });
  }

  publish(topic: string, message: string) {
    this.client.publish(topic, message, (err) => {
      if (err) {
        console.error('Error publishing message:', err);
      } else {
        console.log(`Message published to ${topic}: ${message}`);
      }
    });
  }

  onMessage(callback: (topic: string, message: string) => void) {
    this.client.on('message', (topic, message) => {
      callback(topic, message.toString());
    });
  }
}
