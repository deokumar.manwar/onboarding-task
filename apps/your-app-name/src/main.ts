import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  // cors
  app.enableCors();

  // connect to the port
  const port = process.env.PORT || 3000;
  await app.listen(port).then(() => {
    Logger.log(
      `🚀 Application is running on: http://localhost:${port} written in main.ts`
    );
  });
}

bootstrap();
